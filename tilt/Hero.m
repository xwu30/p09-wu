//
//  Hero.m
//  tilt
//
//  Created by Xian Wu on 5/10/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import "Hero.h"
#import "CategoryMasks.h"

@implementation Hero

+ (instancetype)HeroWithSize:(CGRect) rect {
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    [[UIColor redColor] setStroke];
    [path moveToPoint:CGPointMake(0, 0)];
    [path addLineToPoint:CGPointMake(10, -25)];
    [path addLineToPoint:CGPointMake(0, 25)];
    [path addLineToPoint:CGPointMake(-10, -25)];
    [path closePath];
    [path setLineWidth:2.0];
    
    return NULL;
}

- (CGPathRef) roundInRect: (CGRect)rect
{
    //    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:M_PI];
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    [[UIColor redColor] setStroke];
    [path moveToPoint:CGPointMake(0, 0)];
    [path addLineToPoint:CGPointMake(10, -25)];
    [path addLineToPoint:CGPointMake(0, 25)];
    [path addLineToPoint:CGPointMake(-10, -25)];
    [path closePath];
    [path setLineWidth:2.0];
    
    return path.CGPath;
    
}

@end
