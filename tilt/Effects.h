//
//  Effects.h
//  tilt
//
//  Created by Xian Wu on 5/11/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

@import SpriteKit;

@interface Effects : SKSpriteNode

/**
 * Creates a ball with given radius.
 *
 * @param radius Radius of the ball.
 * @return A new ball.
 */
-(instancetype) initWithRect;

- (instancetype)effectsWithType:(int)type;

- (void)setType:(int)type;

- (int)getType;

@end
