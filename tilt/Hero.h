//
//  Hero.h
//  tilt
//
//  Created by Xian Wu on 5/10/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

@import SpriteKit;

@interface Hero : SKShapeNode

/**
 * Creates a ball with given radius.
 *
 * @param radius Radius of the ball.
 * @return A new ball.
 */
+ (instancetype)HeroWithSize:(CGRect) rect;

@end
