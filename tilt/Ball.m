//
//  Ball.m
//  tilt
//
//  Created by Xian Wu on 5/4/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//
#import "Ball.h"
#import "CategoryMasks.h"

@interface Ball()
@property (nonatomic)  int type1;

@end

@implementation Ball

- (instancetype)initWithRadius:(CGFloat)radius type:(int)type subtype:(int)sub{
    self = [self initWithColor:[SKColor colorWithWhite:0 alpha:0] size:CGSizeMake(1, 1)];
    
    self = [self initWithColor:[SKColor colorWithWhite:0 alpha:0] size:CGSizeMake(1, 1)];
    
    SKShapeNode *ball = [[SKShapeNode alloc]init];
    
    CGRect rect = CGRectMake(0, 0, 2*radius, 2*radius);
    ball.path = [self circleInRect:rect];
    ball.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:radius];
//    earthquake.strokeColor = [SKColor greenColor];
    
    NSString *fileName = [NSString stringWithFormat:@"general.png"];
    
    if(self.type1 == 0){//0 means effects
        if(sub == 1){
            fileName = [NSString stringWithFormat:@"pulseicon.png"];
            ball.physicsBody.categoryBitMask = BallCategory5;
        }else if(sub == 2){
            fileName = [NSString stringWithFormat:@"fireicon.png"];
            ball.physicsBody.categoryBitMask = BallCategory6;
        }else if(sub == 3){
            fileName = [NSString stringWithFormat:@"blackholeicon.png"];
            ball.physicsBody.categoryBitMask = BallCategory7;
        }else if(sub == 4){
            fileName = [NSString stringWithFormat:@"taiji.png"];
            ball.physicsBody.categoryBitMask = BallCategory8;
        }
    }else if(self.type1 == 1){//type is 1 means special
        if(sub == 1){
            fileName = [NSString stringWithFormat:@"springicon.png"];
            ball.physicsBody.categoryBitMask = BallCategory1;
        }else if(sub == 2){
            fileName = [NSString stringWithFormat:@"summericon.png"];
            ball.physicsBody.categoryBitMask = BallCategory2;
        }else if(sub == 3){
            fileName = [NSString stringWithFormat:@"fallicon.png"];
            ball.physicsBody.categoryBitMask = BallCategory3;
        }else if(sub == 4){
            fileName = [NSString stringWithFormat:@"wintericon.png"];
            ball.physicsBody.categoryBitMask = BallCategory4;
        }
    }
    ball.fillTexture = [SKTexture textureWithImageNamed:fileName];

    
    ball.physicsBody.collisionBitMask = 0x0;
    ball.physicsBody.contactTestBitMask = HeroCategory;
    
    
    ball.lineWidth = 0;
    ball.fillColor = [UIColor whiteColor];
    [self addChild:ball];
    return self;
}

- (instancetype)ballWithRadius:(CGFloat)radius type:(int)type subtype:(int)sub{
    [self setType:type];
    return [self initWithRadius:radius type:type subtype:(int)sub];
}

- (CGPathRef) circleInRect:(CGRect)rect
{
    CGRect adjustedRect = CGRectMake(rect.origin.x-rect.size.width/2, rect.origin.y-rect.size.height/2, rect.size.width, rect.size.height);
    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithOvalInRect:adjustedRect];
    return bezierPath.CGPath;
}

- (void) setType:(int)type{
    self.type1 = type;
}

- (int) getType{
    return self.type1;
}

@end
