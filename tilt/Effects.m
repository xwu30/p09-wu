//
//  Effects.m
//  tilt
//
//  Created by Xian Wu on 5/11/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import "Effects.h"
#import "CategoryMasks.h"

@interface Effects()
@property (nonatomic)  int type1;
@property SKSpriteNode *flood;
@property NSArray *floodFrames;
@end

@implementation Effects{

}


-(instancetype) initWithRect{
    
    self = [self initWithColor:[SKColor colorWithWhite:0 alpha:0] size:CGSizeMake(1, 1)];
    
    NSMutableArray *walkFrames = [NSMutableArray array];
    
    SKTextureAtlas *floodAnimatedAtlas = [SKTextureAtlas atlasNamed:@"pulse"];
    
    NSInteger numImages = floodAnimatedAtlas.textureNames.count;
    for (int i=0; i < numImages; i++) {
        NSString *textureName = [NSString stringWithFormat:@"%d", i];
        SKTexture *temp = [floodAnimatedAtlas textureNamed:textureName];
        [walkFrames addObject:temp];
    }
    
    self.floodFrames = walkFrames;
    
    
    SKTexture *temp = self.floodFrames[0];
    self.flood = [SKSpriteNode spriteNodeWithTexture:temp];
    //    self.flood.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    self.flood.zPosition = 0;
    SKAction *repeat = [SKAction repeatActionForever:[SKAction animateWithTextures:self.floodFrames timePerFrame:0.1f resize:NO restore:YES]];
//    SKAction *wait = [SKAction waitForDuration:4.0];
//    SKAction * actionFadeDone = [SKAction fadeOutWithDuration:1.0];
//    SKAction * actionMoveDone = [SKAction removeFromParent];
//    SKAction * actionGroup = [SKAction sequence:@[wait, actionFadeDone, actionMoveDone]];
//    [self.flood runAction:[SKAction group:@[repeat, actionGroup]]];
    [self.flood runAction:repeat];
    //    [self.flood runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:self.floodFrames timePerFrame:0.1f resize:NO restore:YES]] withKey:@"flooding"];
//    CGSize rect = CGSizeMake(188,40);
    CGPathRef path = [self arcInRect];
    self.flood.physicsBody = [SKPhysicsBody bodyWithPolygonFromPath:path];
//    self.flood.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:rect];
    //    self.flood.physicsBody = [SKPhysicsBody bodyWithPolygonFromPath:[self roadInRect]];
    
    self.flood.color = [UIColor whiteColor];
    self.flood.colorBlendFactor = 2.0f;
    self.flood.physicsBody.categoryBitMask = EffectCategory;
    self.flood.physicsBody.dynamic = YES;
    self.flood.physicsBody.affectedByGravity = NO;
    self.flood.physicsBody.collisionBitMask = 0x0;
    self.flood.physicsBody.contactTestBitMask = DotsCategory;
    
    [self addChild:self.flood];
    return self;
    
//    self = [self initWithColor:[SKColor colorWithWhite:0 alpha:0] size:CGSizeMake(1, 1)];
//    
//    SKShapeNode *pulse = [[SKShapeNode alloc]init];
//    pulse.fillColor = [UIColor whiteColor];
//    
//    pulse.path = [self arcInRect];
//    pulse.strokeColor = [SKColor greenColor];
//    pulse.fillColor = [SKColor whiteColor];
////    pulse.position = pos;
//    pulse.physicsBody = [SKPhysicsBody bodyWithPolygonFromPath:pulse.path];
//    pulse.physicsBody.categoryBitMask = EffectCategory;
//    pulse.physicsBody.dynamic = YES;
//    pulse.physicsBody.affectedByGravity = NO;
//    pulse.physicsBody.collisionBitMask = 0x0;
//    pulse.physicsBody.contactTestBitMask = DotsCategory;
//    
//    [self addChild:pulse];
//    
//    return self;
}

-(instancetype) initWithFireroad{
    
    self = [self initWithColor:[SKColor colorWithWhite:0 alpha:0] size:CGSizeMake(1, 1)];
    
    NSMutableArray *walkFrames = [NSMutableArray array];
    
    SKTextureAtlas *floodAnimatedAtlas = [SKTextureAtlas atlasNamed:@"fire"];
    
    NSInteger numImages = floodAnimatedAtlas.textureNames.count;
    for (int i=0; i < numImages; i++) {
        NSString *textureName = [NSString stringWithFormat:@"%d", i];
        SKTexture *temp = [floodAnimatedAtlas textureNamed:textureName];
        [walkFrames addObject:temp];
    }
    
    self.floodFrames = walkFrames;
    
    
    SKTexture *temp = self.floodFrames[0];
    self.flood = [SKSpriteNode spriteNodeWithTexture:temp];
    //    self.flood.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    self.flood.zPosition = 0;
    SKAction *repeat = [SKAction repeatActionForever:[SKAction animateWithTextures:self.floodFrames timePerFrame:0.1f resize:NO restore:YES]];
    SKAction * actionFadeIn = [SKAction fadeInWithDuration:1.0];
    SKAction *wait = [SKAction waitForDuration:4.0];
    SKAction * actionFadeDone = [SKAction fadeOutWithDuration:1.0];
    SKAction * actionMoveDone = [SKAction removeFromParent];
    SKAction * actionGroup = [SKAction sequence:@[actionFadeIn, wait, actionFadeDone, actionMoveDone]];
    [self.flood runAction:[SKAction group:@[repeat, actionGroup]]];
    //    [self.flood runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:self.floodFrames timePerFrame:0.1f resize:NO restore:YES]] withKey:@"flooding"];
    CGSize rect = CGSizeMake(188,40);
    self.flood.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:rect];
    //    self.flood.physicsBody = [SKPhysicsBody bodyWithPolygonFromPath:[self roadInRect]];
    
    self.flood.color = [UIColor whiteColor];
    self.flood.colorBlendFactor = 2.0f;
    self.flood.physicsBody.categoryBitMask = EffectCategory;
    self.flood.physicsBody.dynamic = YES;
    self.flood.physicsBody.affectedByGravity = NO;
    self.flood.physicsBody.collisionBitMask = 0x0;
    self.flood.physicsBody.contactTestBitMask = DotsCategory;
    
    [self addChild:self.flood];
    return self;

}

-(instancetype) initWithEarthQuake{
    self = [self initWithColor:[SKColor colorWithWhite:0 alpha:0] size:CGSizeMake(1, 1)];
    
    NSMutableArray *walkFrames = [NSMutableArray array];
    
    SKTextureAtlas *floodAnimatedAtlas = [SKTextureAtlas atlasNamed:@"blackhole"];
    
    NSInteger numImages = floodAnimatedAtlas.textureNames.count;
    for (int i=0; i < numImages; i++) {
        NSString *textureName = [NSString stringWithFormat:@"%d", i];
        SKTexture *temp = [floodAnimatedAtlas textureNamed:textureName];
        [walkFrames addObject:temp];
    }
    
    self.floodFrames = walkFrames;
    
    
    SKTexture *temp = self.floodFrames[0];
    self.flood = [SKSpriteNode spriteNodeWithTexture:temp];
    //    self.flood.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    self.flood.zPosition = 0;
    SKAction *repeat = [SKAction repeatActionForever:[SKAction animateWithTextures:self.floodFrames timePerFrame:0.1f resize:NO restore:YES]];
    SKAction * actionFadeIn = [SKAction fadeInWithDuration:1.0];
    SKAction *wait = [SKAction waitForDuration:4.0];
    SKAction * actionFadeDone = [SKAction fadeOutWithDuration:1.0];
    SKAction * actionMoveDone = [SKAction removeFromParent];
    SKAction * actionGroup = [SKAction sequence:@[actionFadeIn, wait, actionFadeDone, actionMoveDone]];
    [self.flood runAction:[SKAction group:@[repeat, actionGroup]]];
    //    [self.flood runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:self.floodFrames timePerFrame:0.1f resize:NO restore:YES]] withKey:@"flooding"];
    
    self.flood.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:50.0f];
    //    self.flood.physicsBody = [SKPhysicsBody bodyWithPolygonFromPath:[self roadInRect]];
    
    self.flood.color = [UIColor whiteColor];
    self.flood.colorBlendFactor = 2.0f;
    self.flood.physicsBody.categoryBitMask = EffectCategory;
    self.flood.physicsBody.dynamic = YES;
    self.flood.physicsBody.affectedByGravity = NO;
    self.flood.physicsBody.collisionBitMask = 0x0;
    self.flood.physicsBody.contactTestBitMask = DotsCategory;
    
    [self addChild:self.flood];
    return self;
}

-(instancetype) initWithTaiji{
    self = [self initWithColor:[SKColor colorWithWhite:0 alpha:0] size:CGSizeMake(1, 1)];
    
    NSMutableArray *walkFrames = [NSMutableArray array];
    
    SKTextureAtlas *floodAnimatedAtlas = [SKTextureAtlas atlasNamed:@"taiji"];
    
    NSInteger numImages = floodAnimatedAtlas.textureNames.count;
    for (int i=0; i < numImages; i++) {
        NSString *textureName = [NSString stringWithFormat:@"%d", i];
        SKTexture *temp = [floodAnimatedAtlas textureNamed:textureName];
        [walkFrames addObject:temp];
    }
    
    self.floodFrames = walkFrames;
    
    
    SKTexture *temp = self.floodFrames[0];
    self.flood = [SKSpriteNode spriteNodeWithTexture:temp];
    //    self.flood.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    self.flood.zPosition = 0;
    SKAction *repeat = [SKAction repeatActionForever:[SKAction animateWithTextures:self.floodFrames timePerFrame:0.1f resize:NO restore:YES]];
    SKAction * actionFadeIn = [SKAction fadeInWithDuration:1.0];
    SKAction *wait = [SKAction waitForDuration:4.0];
    SKAction * actionFadeDone = [SKAction fadeOutWithDuration:1.0];
    SKAction * actionMoveDone = [SKAction removeFromParent];
    SKAction * actionGroup = [SKAction sequence:@[actionFadeIn, wait, actionFadeDone, actionMoveDone]];
    [self.flood runAction:[SKAction group:@[repeat, actionGroup]]];
    //    [self.flood runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:self.floodFrames timePerFrame:0.1f resize:NO restore:YES]] withKey:@"flooding"];
    
    self.flood.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:25.0f];
    //    self.flood.physicsBody = [SKPhysicsBody bodyWithPolygonFromPath:[self roadInRect]];
    
    self.flood.color = [UIColor whiteColor];
    self.flood.colorBlendFactor = 2.0f;
    self.flood.physicsBody.categoryBitMask = EffectCategory1;
    self.flood.physicsBody.dynamic = YES;
    self.flood.physicsBody.affectedByGravity = NO;
    self.flood.physicsBody.collisionBitMask = 0x0;
    self.flood.physicsBody.contactTestBitMask = DotsCategory;
    
    [self addChild:self.flood];
    return self;
}

- (instancetype)effectsWithType:(int)type{
    [self setType:type];
    if(type == 0)
        return [self initWithRect];
    else if(type == 1)
        return [self initWithFireroad];
    else if (type == 2)
        return [self initWithEarthQuake];
    else if (type == 3)
        return [self initWithTaiji];
    else
        return NULL;
}


- (CGPathRef) arcInRect
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    [[UIColor blueColor] setStroke];
    [path moveToPoint:CGPointMake(0, -20)];
    //    [path addLineToPoint:CGPointMake(10, -25)];
    [path addArcWithCenter:CGPointMake(0, -25) radius:50 startAngle:M_PI/6
                  endAngle:5*M_PI/6 clockwise:YES];
    
    [path closePath];
    [path setLineWidth:2.0];
    
    return path.CGPath;
    
}

- (CGPathRef) circleInRect:(CGRect)rect
{
    CGRect adjustedRect = CGRectMake(rect.origin.x-rect.size.width/2, rect.origin.y-rect.size.height/2, rect.size.width, rect.size.height);
    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithOvalInRect:adjustedRect];
    return bezierPath.CGPath;
}

- (CGPathRef) roadInRect
{
    CGRect rect = CGRectMake(0, 0, 188, 40);
    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:M_PI];
    return bezierPath.CGPath;
    
}

- (void) setType:(int)type{
    self.type1 = type;
}

- (int) getType{
    return self.type1;
}

@end
