//
//  Ball.h
//  tilt
//
//  Created by Xian Wu on 5/4/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

@import SpriteKit;

@interface Ball : SKSpriteNode

/**
 * Creates a ball with given radius.
 *
 * @param radius Radius of the ball.
 * @return A new ball.
 */
- (instancetype)initWithRadius:(CGFloat)radius type:(int)type subtype:(int)sub;

- (instancetype)ballWithRadius:(CGFloat)radius type:(int)type subtype:(int)sub;

- (void)setType:(int)type;

- (int)getType;

@end
