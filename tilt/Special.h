//
//  Special.h
//  tilt
//
//  Created by Xian Wu on 5/12/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

@import SpriteKit;

@interface Special : SKSpriteNode

/**
 * Creates a ball with given radius.
 *
 * @param radius Radius of the ball.
 * @return A new ball.
 */
-(instancetype) initWithFlood;

- (instancetype)specialWithType:(int)type;

- (void)setType:(int)type;

- (int)getType;

@end
