//
//  MyButton.h
//  tilt
//
//  Created by Xian Wu on 5/13/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface MyButton : SKSpriteNode

@property (nonatomic, copy) void (^callback)();

- (id)initWithUnselectedImageNamed:(NSString *)unselectedImageName selectedImageNamed:(NSString *)selectedImageNamed;

@end
