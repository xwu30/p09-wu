//
//  CategoryMasks.h
//  tilt
//
//  Created by Xian Wu on 5/4/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

typedef NS_OPTIONS(NSUInteger, PhysicalBodyCategory) {
    EmptyCategory    = 0,
    HeroCategory     = 1 << 0,
    DotsCategory     = 1 << 1,
    BallCategory1     = 1 << 2,
    SpecialBallCategory = 1 << 3,
    EffectCategory   = 1 << 4,
    BallCategory2   = 1 << 5,
    BallCategory3   = 1 << 6,
    BallCategory4   = 1 << 7,
    
    BallCategory5   = 1 << 8,
    BallCategory6   = 1 << 9,
    BallCategory7   = 1 << 10,
    BallCategory8   = 1 << 11,
    EffectCategory1   = 1 << 14,
    EffectCategory2   = 1 << 15,
    SceneEdgeCategory= 1 << 20
};
