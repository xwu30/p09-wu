//
//  GameScene.m
//  tilt
//
//  Created by Xian Wu on 5/4/16.
//  Copyright (c) 2016 Xian Wu. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "GameScene.h"
#import "CategoryMasks.h"
#import "Ball.h"
#import "Hero.h"
#import "Effects.h"
#import "Special.h"
#import "MyButton.h"
@import AVFoundation;
@import CoreMotion;

//#import "UIBezierPath+dqd_arrowhead.h"
#pragma mark - self defined variable

#define kScoreHudName @"scoreHud"
#define kHealthHudName @"healthHud"

@interface GameScene()

@property AVAudioPlayer *player;

@property NSArray<SKShapeNode *> *dots;
//@property SKSpriteNode *all;

@property NSArray<Ball *> *balls;
@property SKLabelNode* scoreLabel;
@property SKLabelNode* healthLabel;

@property SKShapeNode *hero;

@property SKSpriteNode *flood;
@property NSArray *floodFrames;
@property SKSpriteNode *backImage;

@property BOOL started;
@property MyButton *helloButton1,*helloButton2,*helloButton3,*helloButton4;
@property MyButton *overButton, *overButton1;

@property SKSpriteNode *overImage;

@property int season;

//@property (nonatomic, strong) UIView *ball;
//@property SKShapeNode *hero1;

@property (nonatomic) NSTimeInterval lastSpawnTimeInterval;
@property (nonatomic) NSTimeInterval lastUpdateTimeInterval;

@property (strong) NSMutableArray* contactQueue;//save for the collision detect
@property NSUInteger score;
@property CGFloat shipHealth;

@end

static inline CGPoint rwAdd(CGPoint a, CGPoint b) {
    return CGPointMake(a.x + b.x, a.y + b.y);
}

static inline CGPoint rwSub(CGPoint a, CGPoint b) {
    return CGPointMake(a.x - b.x, a.y - b.y);
}

static inline CGPoint rwMult(CGPoint a, float b) {
    return CGPointMake(a.x * b, a.y * b);
}

static inline float rwLength(CGPoint a) {
    return sqrtf(a.x * a.x + a.y * a.y);
}


// Makes a vector have a length of 1
static inline CGPoint rwNormalize(CGPoint a) {
    float length = rwLength(a);
    return CGPointMake(a.x / length, a.y / length);
}


@implementation GameScene{
//    Hero *_hero;
    
    SKSpriteNode *_titleImage,*_titleImage1,*_titleImage2;
    
    Ball *_ball;//skill ball
    int _ballType;
    Ball *_specialBall;
    Effects *_effect,*_effect1;
    Special *_special;
    
    CGFloat _direction;
    
    int _MaxDanmu;//max number of danmu of current screen
    int _MinDanmu;//initial number of danmu from begin
    
    int _minDuration;//dots movement time
    int _maxDuration;
    
    int _maxx; // save the screen size x
    int _maxy; // save the screen size y
    int _actualDanmu;
    int _actualBall;
    int _actualSpecial;
    int _pressed;
    CGPoint _target;
    
    CGPoint _offsetDots, _offsetDot;//chasing dots' speed
    
    float _distanceDots;
    
    float _realMoveDurationDots, _realMoveDurationDot;
    
    CGPoint _directionDot,_realdestDot,_dest;
    
    // 7 - Make it shoot far enough to be guaranteed off screen
    CGPoint _shootAmountDot;
    
    // 8 - Add the shoot amount to the current position
    
    
    int _ef1; //paodan
    
}

//- (void)didMoveToView:(SKView *)view {
//    NSLog(@"%f, %f", view.frame.size.width, view.frame.size.height);
//}
#pragma mark - initialize the screen and main elements

-(id)initWithSize:(CGSize)size {
    
    
    if (self = [super initWithSize:size]) {
        
        self.started = false;
        _ef1 = 0;
        _actualBall = 0;
        _actualDanmu = 0;
        _pressed = 0;
        _maxx = self.frame.size.width;
        _maxy = self.frame.size.height;
        NSLog(@"%f, %f", self.frame.size.width, self.frame.size.height);
        self.backImage = [SKSpriteNode spriteNodeWithImageNamed:@"spring.png"];
        self.backImage.xScale = self.frame.size.width/self.backImage.size.width;
        self.backImage.yScale = self.frame.size.height/self.backImage.size.height;
        self.backImage.position = CGPointMake(self.backImage.size.width/2.0f, 2*(self.backImage.size.height/4));
        [self addChild:self.backImage];
        self.backImage.zPosition = -1;

        
        [self setupHud];

        _titleImage = [SKSpriteNode spriteNodeWithImageNamed:@"title1.png"];
        
        _titleImage.position = CGPointMake(self.size.width/2.0f-120, self.size.height*2.0/3.0);
        SKAction *action1 = [SKAction rotateToAngle:M_PI/2 duration:1.0];
        SKAction *action2 = [SKAction rotateToAngle:-1*M_PI/4 duration:1.0];
        SKAction *action3 = [SKAction sequence:@[action1, action2]];
        [_titleImage runAction:[SKAction repeatActionForever:action3]];
        [self addChild:_titleImage];
        _titleImage.zPosition = 1;
        
        _titleImage1 = [SKSpriteNode spriteNodeWithImageNamed:@"title3.png"];
        
        _titleImage1.position = CGPointMake(self.size.width/2.0f+120, self.size.height*2.0/3.0);
        [_titleImage1 runAction:[SKAction repeatActionForever:action3]];
        [self addChild:_titleImage1];
        _titleImage1.zPosition = 1;
        
        _titleImage2 = [SKSpriteNode spriteNodeWithImageNamed:@"title2.png"];
        
        _titleImage2.position = CGPointMake(self.size.width/2.0f, self.size.height*2.0/3.0+80);
        _titleImage2.xScale = 1.2;
        _titleImage2.yScale = 1.2;
        [self addChild:_titleImage2];
        _titleImage2.zPosition = 1;
        
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"bsound" ofType:@"mp3"]];
        self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        self.player.numberOfLoops = -1;
        [self.player play];
        
        
        //button
        self.helloButton1 = [[MyButton alloc] initWithUnselectedImageNamed:@"pickspring" selectedImageNamed:@"picksummer"];
        self.helloButton1.callback = [self hello1];
        self.helloButton1.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
        self.helloButton1.zPosition = 1;
        
        [self addChild:self.helloButton1];
        self.helloButton2 = [[MyButton alloc] initWithUnselectedImageNamed:@"picksummer" selectedImageNamed:@"pickfall"];
        self.helloButton2.callback = [self hello2];
        self.helloButton2.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 - 50);
        self.helloButton2.zPosition = 1;
        
        [self addChild:self.helloButton2];
        self.helloButton3 = [[MyButton alloc] initWithUnselectedImageNamed:@"pickfall" selectedImageNamed:@"pickwinter"];
        self.helloButton3.callback = [self hello3];
        self.helloButton3.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 - 100);
        self.helloButton3.zPosition = 1;
        
        [self addChild:self.helloButton3];
        self.helloButton4 = [[MyButton alloc] initWithUnselectedImageNamed:@"pickwinter" selectedImageNamed:@"pickspring"];
        self.helloButton4.callback = [self hello4];
        self.helloButton4.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 - 150);
        self.helloButton4.zPosition = 1;
        
        [self addChild:self.helloButton4];
        
        self.overButton = [[MyButton alloc] initWithUnselectedImageNamed:@"button1" selectedImageNamed:@"button2"];
        self.overButton.callback = [self over1];
        self.overButton.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2-150);
        self.overButton.zPosition = 2;
        self.overButton.xScale = 0.6;
        self.overButton.yScale = 0.6;
        [self addChild:self.overButton];
        [self.overButton setHidden:YES];
        
        self.overButton1 = [[MyButton alloc] initWithUnselectedImageNamed:@"left" selectedImageNamed:@"right"];
        self.overButton1.callback = [self over2];
        self.overButton1.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2-70);
        self.overButton1.zPosition = 2;
        self.overButton1.xScale = 0.6;
        self.overButton1.yScale = 0.6;
        [self addChild:self.overButton1];
        [self.overButton1 setHidden:YES];
        
        
        self.physicsWorld.gravity = CGVectorMake(0,0);
        self.physicsWorld.contactDelegate = self;
        self.physicsBody.categoryBitMask = SceneEdgeCategory;
        
    }
    return self;
}

-(void)initializeVan{
//    self.started = false;
//    _ef1 = 0;
//    _actualBall = 0;
//    _actualDanmu = 0;
//    _maxx = self.frame.size.width;
//    _maxy = self.frame.size.height;
    [self.backImage removeFromParent];
    if(self.season == 1)
        self.backImage = [SKSpriteNode spriteNodeWithImageNamed:@"spring.png"];
    else if(self.season == 2)
        self.backImage = [SKSpriteNode spriteNodeWithImageNamed:@"summer.png"];
    else if(self.season == 3)
        self.backImage = [SKSpriteNode spriteNodeWithImageNamed:@"fall.png"];
    else if(self.season == 4)
        self.backImage = [SKSpriteNode spriteNodeWithImageNamed:@"winter.png"];
    self.backImage.xScale = self.frame.size.width/self.backImage.size.width;
    self.backImage.yScale = self.frame.size.height/self.backImage.size.height;
    self.backImage.position = CGPointMake(self.backImage.size.width/2.0f, 2*(self.backImage.size.height/4));
    [self addChild:self.backImage];
    self.backImage.zPosition = -1;
    
    [self addScorePad];
    [self addHero];
}

#pragma mark - touch functions and coresponding effects

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{

}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    
    UITouch *touch = [touches anyObject];
    CGPoint positionInScene = [touch locationInNode:self];
    _target = positionInScene;
    // Determine speed
    CGFloat minDuration = .1;
    
    // Create the actions
    CGFloat deltaX = CGRectGetMidX(self.hero.frame) - positionInScene.x;
    CGFloat deltaY = CGRectGetMidY(self.hero.frame) - positionInScene.y;
    
    CGFloat angle = atan2(deltaY, deltaX)+M_PI/2;
    _direction = angle;
    SKAction * actionRotate = [SKAction rotateToAngle:angle duration:minDuration];
    SKAction * actionMove = [SKAction moveTo:CGPointMake(positionInScene.x, positionInScene.y) duration:minDuration];
    SKAction *ggAction = [SKAction group:@[actionMove,actionRotate]];
    if(_ef1 == 1){
        CGPoint offset = rwSub(positionInScene, _effect.position);
        
        CGPoint direction1 = rwNormalize(offset);
        
        // 7 - Make it shoot far enough to be guaranteed off screen
        CGPoint shootAmount = rwMult(direction1, 1000);
        
        // 8 - Add the shoot amount to the current position
        CGPoint realDest = rwAdd(shootAmount, _effect.position);
        
        // 9 - Create the actions
        float velocity = 60.0/1.0;
        float realMoveDuration = self.size.width / velocity;
        if([_effect getType]==0){
            SKAction * actionMove1 = [SKAction moveTo:realDest duration:realMoveDuration];
            SKAction * actionMoveDone = [SKAction removeFromParent];
            [_effect runAction:[SKAction sequence:@[actionRotate, actionMove1, actionMoveDone]]];
        }else if([_effect getType] == 1){
            _effect.zRotation = angle;//firewall angle
            SKAction *wait = [SKAction waitForDuration:4.0];
            SKAction * actionFadeDone = [SKAction fadeOutWithDuration:1.0];
            SKAction * actionMoveDone = [SKAction removeFromParent];
            [_effect runAction:[SKAction sequence:@[wait, actionFadeDone, actionMoveDone]]];
        }else if([_effect getType] == 2){
            SKAction *wait = [SKAction waitForDuration:6.0];
            SKAction * actionFadeDone = [SKAction fadeOutWithDuration:1.0];
            SKAction * actionMoveDone = [SKAction removeFromParent];
            [_effect runAction:[SKAction sequence:@[wait, actionFadeDone, actionMoveDone]]];
        }else if([_effect getType] == 3){
            _effect1 = _effect;
//            [_effect removeFromParent];
        }
        
        _ef1 = 0;
    }
    
    [_effect1 runAction:ggAction];
    [self.hero runAction:ggAction];
}

- (void)addHero{
    self.hero = [[SKShapeNode alloc]init];
    self.hero.zPosition = 2;
    CGRect rect = CGRectMake(0, 0, 32, 16);
    
    
    self.hero.path = [self roundInRect:rect];
    self.hero.strokeColor = [SKColor greenColor];
    self.hero.fillColor = [SKColor redColor];
    self.hero.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame));
    
    self.hero.physicsBody = [SKPhysicsBody bodyWithPolygonFromPath:self.hero.path];
    self.hero.physicsBody.categoryBitMask = HeroCategory;
    self.hero.physicsBody.dynamic = YES;
    self.hero.physicsBody.affectedByGravity = NO;
    self.hero.physicsBody.collisionBitMask = SceneEdgeCategory;
    self.hero.physicsBody.contactTestBitMask = BallCategory1| DotsCategory|SceneEdgeCategory |BallCategory2 |BallCategory3 |BallCategory4 |BallCategory5 |BallCategory6 |BallCategory7 |BallCategory8;
    
    
    
    
    
    _target = self.hero.position;
    [self addChild:self.hero];
}

-(void)addBall{
    
    if(_actualBall <4){
        int efw = arc4random()%15;
        if(efw > 13 && _actualSpecial==0){
            _specialBall = [[Ball alloc]init];
            
            _specialBall = [_specialBall ballWithRadius:15.0 type:1 subtype:self.season];
            _specialBall.position = CGPointMake((arc4random() % _maxx), (arc4random() % _maxy));
            _specialBall.zPosition = 2;
            //        [self.all addChild:_ball];
            self.balls = [self.balls arrayByAddingObject:_specialBall];
            _actualBall++;
            _actualSpecial++;
            [self addChild:_specialBall];
            
        }
        else if(efw < 3){
            _ball = [[Ball alloc]init];
            _ballType = arc4random()%4+1;
            _ball = [_ball ballWithRadius:15.0 type:0 subtype:_ballType];
            _ball.position = CGPointMake((arc4random() % _maxx), (arc4random() % _maxy));
            _ball.zPosition = 2;
            //        [self.all addChild:_ball];
            self.balls = [self.balls arrayByAddingObject:_ball];
            _actualBall++;
            [self addChild:_ball];
        }
    }
}

- (void)addDots {
    
    // Create sprite
    
//     Determine where to spawn the monster along the Y axis
    [self addBall];
    CGRect rect = CGRectMake(0, 0, 16, 16);
    
    //        self.hero = [SKShapeNode shapeNodeWithPath:[self roundInRect:rect] centered:YES];
    
    SKShapeNode *dot = [SKShapeNode shapeNodeWithPath:[self circleInRect:rect] centered:YES];
//    dot.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:dot.frame.size];
    dot.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:8.0];
    dot.physicsBody.categoryBitMask = DotsCategory;
    dot.physicsBody.collisionBitMask = 0x0;
    dot.physicsBody.contactTestBitMask = HeroCategory | EffectCategory | EffectCategory1;
    dot.physicsBody.affectedByGravity = NO;
    dot.physicsBody.dynamic = YES;
    dot.zPosition = 1;
    if(self.season == 1)
        dot.fillColor = [SKColor blueColor];
    else if(self.season == 2)
        dot.fillColor = [SKColor greenColor];
    else if(self.season == 3)
        dot.fillColor = [SKColor redColor];
    else if(self.season == 4)
        dot.fillColor = [SKColor blackColor];
    dot.position = CGPointMake((arc4random() % _maxx), (arc4random() % _maxy));
//    [self.all addChild:dot];
    self.dots = [self.dots arrayByAddingObject:dot];
        _actualDanmu ++;

    // Determine speed of the dots

    
    _offsetDots = rwSub(dot.position, self.hero.position);
    
    _distanceDots = rwLength(_offsetDots);
    
    _realMoveDurationDots = _distanceDots / 20.0;
    
    // Create the actions
    SKAction * actionMove = [SKAction moveTo:self.hero.position duration:_realMoveDurationDots];
    SKAction * actionMoveDone = [SKAction removeFromParent];
    
    SKAction *runMove = [SKAction sequence:@[actionMove, actionMoveDone]];
    [dot runAction: runMove withKey:@"runMove"];
    [self addChild:dot];
    
}

- (void)addDot {
    
    
    CGRect rect = CGRectMake(0, 0, 16, 16);
    
    SKShapeNode *dot = [SKShapeNode shapeNodeWithPath:[self circleInRect:rect] centered:YES];
    dot.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:dot.frame.size];
    dot.physicsBody.categoryBitMask = DotsCategory;
    dot.physicsBody.collisionBitMask = 0x0;
    dot.physicsBody.contactTestBitMask = HeroCategory | EffectCategory | EffectCategory1;
    dot.physicsBody.affectedByGravity = NO;
    dot.physicsBody.dynamic = YES;
    dot.zPosition = 1;
    if(self.season == 1)
        dot.fillColor = [SKColor blueColor];
    else if(self.season == 2)
        dot.fillColor = [SKColor greenColor];
    else if(self.season == 3)
        dot.fillColor = [SKColor redColor];
    else if(self.season == 4)
        dot.fillColor = [SKColor blackColor];
    dot.position = CGPointMake((arc4random() % _maxx), (arc4random() % _maxy));
//    [self.all addChild:dot];
    
    _dest = CGPointMake(arc4random()%_maxx, arc4random()%_maxy);
    
    _offsetDot = rwSub(_dest, dot.position);
    
    
    // 6 - Get the direction of where to shoot
    _directionDot = rwNormalize(_offsetDot);
    
    // 7 - Make it shoot far enough to be guaranteed off screen
    _shootAmountDot = rwMult(_directionDot, 1000);
    
    // 8 - Add the shoot amount to the current position
    _realdestDot = rwAdd(_shootAmountDot, dot.position);
    
    
    int minDuration = 15.0;
    int maxDuration = 30.0;
    int rangeDuration = maxDuration - minDuration;
    int actualDuration = (arc4random() % rangeDuration) + minDuration;
    
    // Create the actions
    SKAction * actionMove = [SKAction moveTo:_realdestDot duration:actualDuration];
        SKAction * actionMoveDone = [SKAction removeFromParent];
    
    SKAction *runMove = [SKAction sequence:@[actionMove, actionMoveDone]];
    [dot runAction: runMove];
    [self addChild:dot];
    
}



#pragma mark - Physics Contact Helpers
-(void)didBeginContact:(SKPhysicsContact *)contact {
    [self.contactQueue addObject:contact];
}
-(void)handleContact:(SKPhysicsContact*)contact {
    // Ensure you haven't already handled this contact and removed its nodes
    if (!contact.bodyA.node.parent || !contact.bodyB.node.parent) return;
    
    SKPhysicsBody *firstBody, *secondBody;
    
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    } else {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    
    if (firstBody.categoryBitMask != HeroCategory && firstBody.categoryBitMask != DotsCategory) {
        return;
    }
    SKAction *sound;
    CGPoint tar;
    SKAction * actionMove5;
    SKAction * actionMove6;
    if(firstBody.categoryBitMask == HeroCategory){
        switch (secondBody.categoryBitMask) {
            case DotsCategory:
                //            firstBody.velocity = increaseBy(firstBody.velocity, 1000);
                [self adjustShipHealthBy:-0.334f];
                if (self.shipHealth <= 0.0f) {
                    //2
                    [contact.bodyA.node removeFromParent];
                    [contact.bodyB.node removeFromParent];
                    [self endGame:1];
                } else {
                    //3
                    [contact.bodyB.node removeFromParent];
                    
                }
                break;
            case BallCategory1:
//                _ef1 = 1;
                _actualSpecial--;
                sound = [SKAction playSoundFileNamed:@"throughneedle2.mp3" waitForCompletion:YES];
                [self runAction:sound];
                [secondBody.node removeFromParent];
                _actualBall--;
                _special = [[Special alloc]init];
                _special = [_special specialWithType:self.season];
                    _special.position = CGPointMake(190, 110);
                _special.zPosition = 1;
                [_special runAction:[SKAction fadeInWithDuration:1.0]];
                [self addChild:_special];
                
                break;
            case BallCategory2:
                _actualSpecial--;
                sound = [SKAction playSoundFileNamed:@"throughneedle2.mp3" waitForCompletion:YES];
                [self runAction:sound];
                [secondBody.node removeFromParent];
                _actualBall--;
                _special = [[Special alloc]init];
                _special = [_special specialWithType:self.season];
//                if(self.season == 1)
//                    _special.position = CGPointMake(190, 110);
//                else if(self.season == 2)
                    _special.position = CGPointMake(170, 520);
                _special.zPosition = 1;
                [_special runAction:[SKAction fadeInWithDuration:1.0]];
                [self addChild:_special];
                
                break;
            case BallCategory3:
                _actualSpecial--;
                sound = [SKAction playSoundFileNamed:@"throughneedle2.mp3" waitForCompletion:YES];
                [self runAction:sound];
                [secondBody.node removeFromParent];
                _actualBall--;
                _special = [[Special alloc]init];
                _special = [_special specialWithType:self.season];
                _special.position = CGPointMake(90, 300);
                _special.zPosition = 1;
                tar = CGPointMake(330, 300);
                actionMove5 =[SKAction fadeInWithDuration:1.0];
                actionMove6 = [SKAction moveTo:tar duration:6.0];
                [_special runAction:[SKAction sequence:@[actionMove5,actionMove6]]];
                [self addChild:_special];
                
                break;
            case BallCategory4:
                _actualSpecial--;
                sound = [SKAction playSoundFileNamed:@"throughneedle2.mp3" waitForCompletion:YES];
                [self runAction:sound];
                [secondBody.node removeFromParent];
                _actualBall--;
                _special = [[Special alloc]init];
                _special = [_special specialWithType:self.season];
                _special.position = CGPointMake(_maxx/2, 520);

                tar = CGPointMake(190, 100);
                actionMove5 =[SKAction fadeInWithDuration:1.0];
                actionMove6 = [SKAction moveTo:tar duration:6.0];
                [_special runAction:[SKAction sequence:@[actionMove5,actionMove6]]];
                _special.zPosition = 1;
                [self addChild:_special];
                
                break;
            case BallCategory5:
                _ef1 = 1;
                sound = [SKAction playSoundFileNamed:@"throughneedle2.mp3" waitForCompletion:YES];
                [self runAction:sound];
                [secondBody.node removeFromParent];
                _actualBall--;
                _effect = [[Effects alloc]init];
                _effect = [_effect effectsWithType:0];
                
                _effect.position = self.hero.position;
                [_effect runAction:[SKAction fadeInWithDuration:1.0]];
                [self addChild:_effect];
                
                break;
            case BallCategory6:
                _ef1 = 1;
                sound = [SKAction playSoundFileNamed:@"throughneedle2.mp3" waitForCompletion:YES];
                [self runAction:sound];
                [secondBody.node removeFromParent];
                _actualBall--;
                _effect = [[Effects alloc]init];
                _effect = [_effect effectsWithType:1];
                
                _effect.position = self.hero.position;
                [_effect runAction:[SKAction fadeInWithDuration:1.0]];
                [self addChild:_effect];
                
                break;
            case BallCategory7:
                _ef1 = 1;
                sound = [SKAction playSoundFileNamed:@"throughneedle2.mp3" waitForCompletion:YES];
                [self runAction:sound];
                [secondBody.node removeFromParent];
                _actualBall--;
                _effect = [[Effects alloc]init];
                _effect = [_effect effectsWithType:2];
                
                _effect.position = self.hero.position;
                [_effect runAction:[SKAction fadeInWithDuration:1.0]];
                [self addChild:_effect];
                
                break;
            case BallCategory8:
                _ef1 = 1;
                sound = [SKAction playSoundFileNamed:@"throughneedle2.mp3" waitForCompletion:YES];
                [self runAction:sound];
                [secondBody.node removeFromParent];
                _actualBall--;
                _effect = [[Effects alloc]init];
                _effect = [_effect effectsWithType:3];
                
                _effect.position = self.hero.position;
                [_effect runAction:[SKAction fadeInWithDuration:1.0]];
                [self addChild:_effect];
                
                break;
            case EffectCategory:
                break;
            default:
                break;
        }
    }else if(firstBody.categoryBitMask == DotsCategory){
        SKAction * actionFadeDone = [SKAction fadeOutWithDuration:0.2];
        SKAction * actionMoveDone = [SKAction removeFromParent];
        int tt = arc4random()%3;
        
        switch (secondBody.categoryBitMask) {
            case DotsCategory:
                //            firstBody.velocity = increaseBy(firstBody.velocity, 1000);
                break;
            case BallCategory1:
                
                break;
            case EffectCategory:
                if(tt == 0)
                    [self adjustScoreBy:100*self.season];
                else if(tt == 1)
                    [self adjustScoreBy:150*self.season];
                else if(tt == 2)
                    [self adjustScoreBy:233*self.season];
                _actualDanmu--;
                [firstBody.node runAction:[SKAction sequence:@[ actionFadeDone, actionMoveDone]]];
//                [firstBody.node runAction:[SKAction fadeOutWithDuration:1.0]];
                sound = [SKAction playSoundFileNamed:@"buble.mp3" waitForCompletion:NO];
                [self runAction:sound];
                break;
            case EffectCategory1:
//                if(tt == 0)
//                    [self adjustScoreBy:100];
//                else if(tt == 1)
//                    [self adjustScoreBy:150];
//                else if(tt == 2)
                    [self adjustScoreBy:233*self.season];
                _actualDanmu--;
                [firstBody.node runAction:actionMoveDone];
                //                [firstBody.node runAction:[SKAction fadeOutWithDuration:1.0]];
                sound = [SKAction playSoundFileNamed:@"buble.mp3" waitForCompletion:NO];
                [self runAction:sound];
                break;
            default:
                break;
        }
    }
}



#pragma mark - update scene
-(void)processContactsForUpdate:(NSTimeInterval)currentTime {
    for (SKPhysicsContact* contact in [self.contactQueue copy]) {
        [self handleContact:contact];
        [self.contactQueue removeObject:contact];
    }
}

- (void)updateWithTimeSinceLastUpdate:(CFTimeInterval)timeSinceLast {
    int i = 0;
    self.lastSpawnTimeInterval += timeSinceLast;
    if (self.lastSpawnTimeInterval > 1) {
        self.lastSpawnTimeInterval = 0;
//        [monsternode removeActionForkey: @"runMove"];
        for(; i < _actualDanmu;i++){
            [self.dots[i] removeActionForKey: @"runMove"];
            _offsetDots = rwSub(self.dots[i].position, self.hero.position);
            
            _distanceDots = rwLength(_offsetDots);
            if(self.season == 1)
                _realMoveDurationDots = _distanceDots / 15.0;
            else if(self.season == 2)
                _realMoveDurationDots = _distanceDots / 20.0;
            else if(self.season == 3)
                _realMoveDurationDots = _distanceDots / 25.0;
            else if(self.season == 4)
                _realMoveDurationDots = _distanceDots / 30.0;
            // Create the actions

            SKAction * actionMove = [SKAction moveTo:_target duration:_realMoveDurationDots];
            SKAction * actionMoveDone = [SKAction removeFromParent];
            
            SKAction *runMove = [SKAction sequence:@[actionMove, actionMoveDone]];
            [self.dots[i] runAction: runMove withKey:@"runMove"];
        }
        int aj = arc4random()%2;
        if(aj == 0)
            [self addDots];
        else
            [self addDot];
    }
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    // Handle time delta.
    // If we drop below 60fps, we still want everything to move the same distance.
    if(self.started){
    [self processContactsForUpdate:currentTime];//deal with the contact update function
    
    CFTimeInterval timeSinceLast = currentTime - self.lastUpdateTimeInterval;
    self.lastUpdateTimeInterval = currentTime;
    if (timeSinceLast > 1) { // more than a second since last update
        timeSinceLast = 1.0 / 60.0;
        self.lastUpdateTimeInterval = currentTime;
//        [self.mons[1] removeActionForKey: @"runMove"];
        
    }

    [self updateWithTimeSinceLastUpdate:timeSinceLast];
    }
}


#pragma mark = draw the path for wanted graph
- (CGPathRef) roundInRect: (CGRect)rect
{
//    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:M_PI];
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    [[UIColor redColor] setStroke];
    [path moveToPoint:CGPointMake(0, 0)];
    [path addLineToPoint:CGPointMake(7, -14)];
    [path addLineToPoint:CGPointMake(0, 14)];
        [path addLineToPoint:CGPointMake(-7, -14)];
    [path closePath];
    [path setLineWidth:2.0];
    
    return path.CGPath;
    
}

- (CGPathRef) arcInRect: (CGRect)rect
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    [[UIColor blueColor] setStroke];
    [path moveToPoint:CGPointMake(0, -20)];
//    [path addLineToPoint:CGPointMake(10, -25)];
    [path addArcWithCenter:CGPointMake(0, -25) radius:50 startAngle:M_PI/6
                  endAngle:5*M_PI/6 clockwise:YES];
    
    [path closePath];
    [path setLineWidth:2.0];
    
    return path.CGPath;
    
}

- (CGPathRef) circleInRect:(CGRect)rect
{
    CGRect adjustedRect = CGRectMake(rect.origin.x-rect.size.width/2, rect.origin.y-rect.size.height/2, rect.size.width, rect.size.height);
    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithOvalInRect:adjustedRect];
    return bezierPath.CGPath;
}

#pragma mark - add nsarry and other array elements
-(void)addScorePad{
    self.balls = [NSArray array];
    self.dots = [NSArray array];
    self.contactQueue = [NSMutableArray array];
    _actualDanmu = 0;
}

-(void)destoryScorePad{
    int i = 0;
    [self.hero removeFromParent];
    for(;i<_actualBall;i++){
        [self.balls[i] removeFromParent];
    }
    for(i = 0;i<_actualDanmu;i++){
        [self.dots[i] removeFromParent];
    }
    
    self.balls = NULL;
    _actualDanmu = 0;
    self.dots = NULL;
    _actualBall = 0;
    self.contactQueue = NULL;
}

#pragma mark - setup HUD

-(void)setupHud {
    self.scoreLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    //1
    self.scoreLabel.name = kScoreHudName;
    self.scoreLabel.fontSize = 15;
    //2
    self.scoreLabel.fontColor = [SKColor greenColor];
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %04u", 0];
    //3
    self.scoreLabel.position = CGPointMake(20 + self.scoreLabel.frame.size.width/2, self.size.height - (20 + self.scoreLabel.frame.size.height/2));
    self.scoreLabel.zPosition = 5;
    [self addChild:self.scoreLabel];
    
    self.healthLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    //4
    self.healthLabel.name = kHealthHudName;
    self.healthLabel.fontSize = 15;
    //5
    self.healthLabel.fontColor = [SKColor redColor];
    self.shipHealth = 1.0f;
    self.healthLabel.text = [NSString stringWithFormat:@"Health: %.1f%%", self.shipHealth * 100.0f];
    //6
    self.healthLabel.position = CGPointMake(self.size.width - self.healthLabel.frame.size.width/2 - 20, self.size.height - (20 + self.healthLabel.frame.size.height/2));
    self.healthLabel.zPosition = 5;
    [self addChild:self.healthLabel];
}

#pragma mark - HUD helper
-(void)adjustScoreBy:(NSUInteger)points {
    self.score += points;
    SKLabelNode* score = (SKLabelNode*)[self childNodeWithName:kScoreHudName];
    score.text = [NSString stringWithFormat:@"Score: %04lu", (unsigned long)self.score];
}

-(void)adjustShipHealthBy:(CGFloat)healthAdjustment {
    //1
    self.shipHealth = MAX(self.shipHealth + healthAdjustment, 0);
    
    SKLabelNode* health = (SKLabelNode*)[self childNodeWithName:kHealthHudName];
    health.text = [NSString stringWithFormat:@"Health: %.1f%%", self.shipHealth * 100];
}

#pragma mark--button function
- (void(^)())hello1
{
    return ^(){
        self.started = true;
        self.season = 1;
        [_titleImage removeFromParent];
        [_titleImage1 removeFromParent];
        [_titleImage2 removeFromParent];
        [self.helloButton1 setHidden:YES];
        [self.helloButton2 setHidden:YES];
        [self.helloButton3 setHidden:YES];
        [self.helloButton4 setHidden:YES];
        [self initializeVan];
    };
}

- (void(^)())hello2
{
    return ^(){
        self.started = true;
        self.season = 2;
        [_titleImage removeFromParent];
        [_titleImage1 removeFromParent];
        [_titleImage2 removeFromParent];
        [self.helloButton1 setHidden:YES];
        [self.helloButton2 setHidden:YES];
        [self.helloButton3 setHidden:YES];
        [self.helloButton4 setHidden:YES];
        [self initializeVan];
    };
}

- (void(^)())hello3
{
    return ^(){
        self.started = true;
        self.season = 3;
        [_titleImage removeFromParent];
        [_titleImage1 removeFromParent];
        [_titleImage2 removeFromParent];
        [self.helloButton1 setHidden:YES];
        [self.helloButton2 setHidden:YES];
        [self.helloButton3 setHidden:YES];
        [self.helloButton4 setHidden:YES];
        [self initializeVan];
    };
}

- (void(^)())hello4
{
    return ^(){
        self.started = true;
        self.season = 4;
        [_titleImage removeFromParent];
        [_titleImage1 removeFromParent];
        [_titleImage2 removeFromParent];
        [self.helloButton1 setHidden:YES];
        [self.helloButton2 setHidden:YES];
        [self.helloButton3 setHidden:YES];
        [self.helloButton4 setHidden:YES];
        [self initializeVan];
    };
}

- (void(^)())over1
{
    return ^(){
//        [self.overButton setHidden:YES];
//        [self.overImage setHidden:YES];
////        //        [_grid setHidden:NO];
//        [self.helloButton1 setHidden:NO];
//        [self.helloButton2 setHidden:NO];
//        [self.helloButton3 setHidden:NO];
//        [self.helloButton4 setHidden:NO];
//        self.scoreLabel.text = [NSString stringWithFormat:@"Score: %04u", 0];
        GameScene* gameOverScene = [[GameScene alloc] initWithSize:self.size];
        [self.view presentScene:gameOverScene transition:[SKTransition fadeWithDuration:2.0]];
//        [self.player play];
    };
}

- (void(^)())over2
{
    return ^(){
        
        _pressed = _pressed%4+1;
        [self.overImage removeFromParent];
        NSString *fileName = [NSString stringWithFormat:@"end%d.png", _pressed];
        self.overImage = [SKSpriteNode spriteNodeWithImageNamed:fileName];
        float ratio = 0.85*self.frame.size.width/self.overImage.size.width;
        self.overImage.xScale = ratio;
        self.overImage.yScale = ratio;
        self.overImage.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2+50);
        [self addChild:self.overImage];
        self.overImage.zPosition = 1;
        
    };
}
#pragma mark--gameover setup
- (void)endGame:(int)status
{
    self.started = false;
//    [self destoryScorePad];
    self.shipHealth = 0.0f;
    self.healthLabel.text = [NSString stringWithFormat:@"Health: %.1f%%", self.shipHealth * 100.0f];

    NSString *fileName = [NSString stringWithFormat:@"bb%d.png", status + 1];
    [self.player pause];
    if(status == 0){
//        _creditCookie++;
        [self runAction:[SKAction playSoundFileNamed:@"win"
                                   waitForCompletion:YES]];
    }else{
//        _creditCookie = 0;
        [self runAction:[SKAction playSoundFileNamed:@"lose"
                                   waitForCompletion:YES]];
    }
    
    self.overImage = [SKSpriteNode spriteNodeWithImageNamed:fileName];
    float ratio = 0.85*self.frame.size.width/self.overImage.size.width;
    self.overImage.xScale = ratio;
    self.overImage.yScale = ratio;
    self.overImage.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2+10);
    [self addChild:self.overImage];
    self.overImage.zPosition = 1;
    [self.overButton setHidden:NO];
    [self.overButton1 setHidden:NO];
}

@end
